package com.quiz.QuizProgram;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class QuizService {

    @Autowired
    private QuizRepository quizRepository;


    public List<QuizInfo> getAllQuizInfo() {
        List<QuizInfo> quiz = new ArrayList<>();
        quizRepository.findAll().forEach(quiz::add);
        return quiz;
    }

    public Optional<QuizInfo> getQuizInfo(int id) {
        return quizRepository.findById(id);
    }

    public void addQuiz(QuizInfo quiz) {
        quizRepository.save(quiz);
    }

    public void updateQuiz(int id, QuizInfo quiz) {
        quizRepository.save(quiz);

    }

    public void deleteQuiz(int id) {
        quizRepository.deleteById(id);
    }
}
