package com.quiz.QuizProgram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

@RestController
public class QuizController {

    @Autowired
    private QuizService quizService;

    //Get All
    public List<QuizInfo> getALLQuiz() {
        return quizService.getAllQuizInfo();
    }

    //Get One
    public Optional<QuizInfo> getQuiz(int id) {
        return quizService.getQuizInfo(id);
    }

    //Add quiz
    public void addQuiz(QuizInfo quiz) {
        quizService.addQuiz(quiz);
    }

    //update quiz
    public void uodateQuiz(QuizInfo quiz, int id) {
        quizService.updateQuiz(id, quiz);

    }

    //delete quiz
    public void deleteQuiz(int id) {
        quizService.deleteQuiz(id);
    }


}
