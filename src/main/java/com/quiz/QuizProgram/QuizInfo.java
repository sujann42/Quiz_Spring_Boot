package com.quiz.QuizProgram;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class QuizInfo {

    @Id
    private int questionNo;
    private String question;
    private String options;
    private char answer;
    private String reason;

    public QuizInfo(int questionNo, String question, String options, char answer, String reason) {
        this.questionNo = questionNo;
        this.question = question;
        this.options = options;
        this.answer = answer;
        this.reason = reason;
    }

    public QuizInfo() {
    }

    public int getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(int questionNo) {
        this.questionNo = questionNo;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public char getAnswer() {
        return answer;
    }

    public void setAnswer(char answer) {
        this.answer = answer;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
